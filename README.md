# Optimal Join Order

Finds optimal join order of relations.

## Building

### Prerequisites

- [Rust](https://www.rust-lang.org/)

### Compilation

Execute following command in the project root directory:

```sh
cargo build --release
```

## Running Tests

```sh
cargo test
```

## Running

```sh
./target/release/ojo
```

Then, put relation information in the following format:

```txt
RELATION_1_NAME RELATION_1_SIZE, RELATION_2_NAME RELATION_2_SIZE, RELATION_3_NAME RELATION_3_SIZE
```

For example:

```txt
A 12, B 18, C 15
```

Meaning relation `A` has `12` tuples, `B` has `18` tuples, `C` has `15` tuples.

_ojo_ will print the optimal order of received relations join operations, separated by `>` character in the first line, followed by total cost of operations, as defined, in the next line.

If any error occurred during the reading input, or parsing it, it will be reported before any calculations.
