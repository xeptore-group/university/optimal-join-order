fn printable_order<'a>(sorted: &'a Vec<&ojo::Relation>) -> String {
    sorted
        .iter()
        .map(|relation| relation.name.as_str())
        .collect::<Vec<_>>()
        .join(" > ")
}

fn cost_till_index<'a>(sorted: &'a Vec<&ojo::Relation>, index: usize) -> usize {
    sorted.iter().take(index).fold(1, |acc, relation| acc * relation.num_rows)
        * (sorted.get(index).unwrap().num_rows as f64 / 20f64).ceil() as usize
}

fn total_cost<'a>(sorted: &'a Vec<&ojo::Relation>) -> usize {
    (0..sorted.len())
        .into_iter()
        .skip(1)
        .map(|index| cost_till_index(sorted, index))
        .sum::<usize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_printable_order() {
        assert_eq!(
            printable_order(&vec![
                &ojo::Relation {
                    name: String::from("A"),
                    num_rows: 12
                },
                &ojo::Relation {
                    name: String::from("B"),
                    num_rows: 16
                },
                &ojo::Relation {
                    name: String::from("C"),
                    num_rows: 30
                }
            ]),
            "A > B > C"
        );
    }

    #[test]
    fn test_cost_till_index() {
        assert_eq!(
            cost_till_index(
                &vec![
                    &ojo::Relation {
                        name: String::from("A"),
                        num_rows: 12
                    },
                    &ojo::Relation {
                        name: String::from("B"),
                        num_rows: 16
                    },
                    &ojo::Relation {
                        name: String::from("C"),
                        num_rows: 30
                    }
                ],
                2
            ),
            384
        );
    }

    #[test]
    fn test_total_cost() {
        assert_eq!(
            total_cost(&vec![
                &ojo::Relation {
                    name: String::from("A"),
                    num_rows: 12
                },
                &ojo::Relation {
                    name: String::from("B"),
                    num_rows: 16
                },
                &ojo::Relation {
                    name: String::from("C"),
                    num_rows: 30
                }
            ]),
            396
        );
    }
}

fn read_relations() -> Vec<ojo::Relation> {
    ojo::read_relations()
        .expect("Could not read input successfully")
        .into_iter()
        .map(|item| match item {
            Ok(relation) => relation,
            Err(ojo::ParseError::ItemParseError) => {
                panic!("Could not parse relation item")
            }
            Err(ojo::ParseError::NumberParseError) => {
                panic!("Could not convert relation size")
            }
        })
        .collect::<Vec<_>>()
}

fn main() {
    let relations = read_relations();

    let sorted = ojo::sort(&relations);

    println!("{}", printable_order(&sorted));

    println!("{}", total_cost(&sorted));
}
