use std::io;

#[derive(PartialEq, Debug)]
pub struct Relation {
    pub name: String,
    pub num_rows: usize,
}

fn merge<'a>(left: &[&'a Relation], right: &[&'a Relation]) -> Vec<&'a Relation> {
    let mut i = 0;
    let mut j = 0;
    let mut merged = Vec::new();

    while i < left.len() && j < right.len() {
        if left[i].num_rows < right[j].num_rows {
            merged.push(left[i]);
            i += 1;
        } else {
            merged.push(right[j]);
            j += 1;
        }
    }

    if i < left.len() {
        while i < left.len() {
            merged.push(left[i]);
            i += 1;
        }
    }

    if j < right.len() {
        while j < right.len() {
            merged.push(right[j]);
            j += 1;
        }
    }

    merged
}

fn merge_sort<'a>(vec: &'a [Relation]) -> Vec<&'a Relation> {
    if vec.len() < 2 {
        let mut out: Vec<&'a Relation> = Vec::with_capacity(vec.len());
        for item in vec.iter() {
            out.push(item);
        }
        out
    } else {
        let size = vec.len() / 2;
        let left = merge_sort(&vec[0..size]);
        let right = merge_sort(&vec[size..]);
        let merged = merge(&left, &right);

        merged
    }
}

pub fn sort<'a>(vec: &'a [Relation]) -> Vec<&'a Relation> {
    merge_sort(vec)
}

fn read_line() -> Result<String, std::io::Error> {
    let mut line = String::new();

    io::stdin().read_line(&mut line).and(Ok(line))
}

#[derive(Debug, Clone)]
pub enum ParseError {
    NumberParseError,
    ItemParseError,
}

fn parse_item<'a>(item: &'a str) -> Result<Relation, ParseError> {
    let parts = item.split_whitespace().collect::<Vec<_>>();
    if parts.len() != 2 {
        return Err(ParseError::ItemParseError);
    }

    let relation_name = parts[0];
    let relation_tuples = parts[1]
        .parse::<usize>()
        .or(Err(ParseError::NumberParseError))?;

    Ok(Relation {
        name: String::from(relation_name),
        num_rows: relation_tuples,
    })
}

pub fn read_relations() -> Result<Vec<Result<Relation, ParseError>>, std::io::Error> {
    let out = read_line()?
        .as_str()
        .split(',')
        .map(|item| parse_item(&item))
        .collect::<Vec<_>>();

    Ok(out)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_merge() {
        let left = [
            &Relation {
                name: String::from("B"),
                num_rows: 16,
            },
            &Relation {
                name: String::from("C"),
                num_rows: 30,
            },
            &Relation {
                name: String::from("A"),
                num_rows: 12,
            },
        ];
        let right = [
            &Relation {
                name: String::from("E"),
                num_rows: 10,
            },
            &Relation {
                name: String::from("F"),
                num_rows: 8,
            },
            &Relation {
                name: String::from("G"),
                num_rows: 63,
            },
        ];
        assert_eq!(
            merge(&left, &right),
            vec![
                &Relation {
                    name: String::from("E"),
                    num_rows: 10
                },
                &Relation {
                    name: String::from("F"),
                    num_rows: 8
                },
                &Relation {
                    name: String::from("B"),
                    num_rows: 16
                },
                &Relation {
                    name: String::from("C"),
                    num_rows: 30
                },
                &Relation {
                    name: String::from("A"),
                    num_rows: 12
                },
                &Relation {
                    name: String::from("G"),
                    num_rows: 63
                },
            ],
        );
    }

    #[test]
    fn test_merge_sort() {
        assert_eq!(
            merge_sort(&vec![
                Relation {
                    name: String::from("B"),
                    num_rows: 16
                },
                Relation {
                    name: String::from("C"),
                    num_rows: 30
                },
                Relation {
                    name: String::from("A"),
                    num_rows: 12
                },
            ]),
            vec![
                &Relation {
                    name: String::from("A"),
                    num_rows: 12
                },
                &Relation {
                    name: String::from("B"),
                    num_rows: 16
                },
                &Relation {
                    name: String::from("C"),
                    num_rows: 30
                },
            ],
        );
    }

    #[test]
    fn test_parse_item() {
        let result = parse_item("patented table standard CMS plugin");
        match result {
            Err(ParseError::ItemParseError) => {}
            _ => assert_eq!(false, true),
        };
        let result = parse_item("patented x35s3x2");
        match result {
            Err(ParseError::NumberParseError) => {}
            _ => assert_eq!(false, true),
        };
    }
}
